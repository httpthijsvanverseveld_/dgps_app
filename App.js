import React from "react";
import {createBottomTabNavigator, createAppContainer} from "react-navigation";
import Home from "./source/screens/Home";
import Drivers from "./source/screens/Drivers";
import NavigationBadge from "./source/components/NavigationBadge/NavigationBadge";
import Races from "./source/screens/Races";
import Constructors from "./source/screens/Constructors";

const AppNavigator = createBottomTabNavigator({
    Home: {
        screen: Home,
    },
    Races: {
        screen: Races,
    },
    Drivers: {
        screen: Drivers,
    },
    Constructors: {
        screen: Constructors,
    },
},
    {
        tabBarOptions: {
            activeTintColor: '#DC0000',
            inactiveTintColor: 'gray',
        },
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({focused, horizontal, tintColor}) => {
                const {routeName} = navigation.state;
                let iconName;
                let badge = 0;
                switch (routeName) {
                    case 'Home':
                        iconName = 'home';
                        break;
                    case 'Drivers':
                        iconName = 'trophy';
                        break;
                    case 'Races':
                        iconName = 'flag';
                        break;
                    case 'Constructors':
                        iconName = 'team';
                }

                return <NavigationBadge name={iconName} focused={focused} color={tintColor} badgeCount={badge}/>;
            },
        }),
    });

export default createAppContainer(AppNavigator);
