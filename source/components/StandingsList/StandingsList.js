import * as React from 'react';
import {Content, List, ListItem, Tab, Tabs} from 'native-base';
import StandingsRow from "./StandingsRow";
import {Colors} from "../../utils/colors";

interface Props {
    league: 'gp1' | 'gp2' | 'gp3' | 'f1';
    type: 'drivers' | 'constructors';
}

const fakeData = {
    gp1: {
        1: {
            name: 'x iTzThijsiii x',
            team: 'McLaren',
            points: 50,
        },
        2: {
            name: 'DGP Speedrazorr',
            team: 'Renault',
            points: 45,
        },
        3: {
            name: 'Sandrog',
            team: 'Ferrari',
            points: 40,
        },
        4: {
            name: 'x iTzThijsiii x',
            team: 'McLaren',
            points: 50,
        },
        5: {
            name: 'DGP Speedrazorr',
            team: 'Renault',
            points: 45,
        },
        6: {
            name: 'Sandrog',
            team: 'Ferrari',
            points: 40,
        },
        7: {
            name: 'x iTzThijsiii x',
            team: 'McLaren',
            points: 50,
        },
        8: {
            name: 'DGP Speedrazorr',
            team: 'Renault',
            points: 45,
        },
        9: {
            name: 'Sandrog',
            team: 'Ferrari',
            points: 40,
        },
        10: {
            name: 'x iTzThijsiii x',
            team: 'McLaren',
            points: 50,
        },
        11: {
            name: 'DGP Speedrazorr',
            team: 'Renault',
            points: 45,
        },
        12: {
            name: 'Sandrog',
            team: 'Ferrari',
            points: 40,
        },
        13: {
            name: 'x iTzThijsiii x',
            team: 'McLaren',
            points: 50,
        },
        14: {
            name: 'DGP Speedrazorr',
            team: 'Renault',
            points: 45,
        },
        15: {
            name: 'Sandrog',
            team: 'Ferrari',
            points: 40,
        },
        16: {
            name: 'x iTzThijsiii x',
            team: 'McLaren',
            points: 50,
        },
        17: {
            name: 'DGP Speedrazorr',
            team: 'Renault',
            points: 45,
        },
        18: {
            name: 'Sandrog',
            team: 'Ferrari',
            points: 40,
        },
        19: {
            name: 'x iTzThijsiii x',
            team: 'McLaren',
            points: 50,
        },
        20: {
            name: 'DGP Speedrazorr',
            team: 'Renault',
            points: 45,
        },
    },
    gp2: {
        1: {
            name: 'x iTzThijsiii x',
            team: 'McLaren',
            points: 50,
        },
        2: {
            name: 'DGP Speedrazorr',
            team: 'Renault',
            points: 45,
        },
        3: {
            name: 'Sandrog',
            team: 'Ferrari',
            points: 40,
        }
    },
    gp3: {
        1: {
            name: 'x iTzThijsiii x',
            team: 'McLaren',
            points: 50,
        },
        2: {
            name: 'DGP Speedrazorr',
            team: 'Renault',
            points: 45,
        },
        3: {
            name: 'Sandrog',
            team: 'Ferrari',
            points: 40,
        }
    },
    f1: {
        1: {
            name: 'x iTzThijsiii x',
            team: 'McLaren',
            points: 50,
        },
        2: {
            name: 'DGP Speedrazorr',
            team: 'Renault',
            points: 45,
        },
        3: {
            name: 'Sandrog',
            team: 'Ferrari',
            points: 40,
        }
    }
}

class StandingsList extends React.Component<Props> {
    render() {
        const data = fakeData[this.props.league];

        return (
            <Content style={{ backgroundColor: Colors.dark, padding: 0, margin: 0 }}>
                <List>
                    { Object.keys(data).map(position =>
                        <ListItem style={{ padding: 0, marginRight: -18, marginLeft: -18, marginTop: -10, marginBottom: -10 }} first={{ marginTop: 0, }} noIndent noBorder>
                            <StandingsRow position={position} {...data[position]} />
                        </ListItem>
                    )}
                </List>
            </Content>
        );
    }
}

export default StandingsList;
