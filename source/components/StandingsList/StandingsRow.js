import * as React from 'react';
import {View, Text} from 'react-native';
import uuid from 'uuid/v4';

interface Props {
    position: number;
    team: string;
    name: string;
    points: number;
}

function getBackgroundColor(team) {
    switch(team) {
        case 'Ferrari':
            return '#DC0000';
        case 'McLaren':
            return '#FF8700';
        case 'Renault':
            return '#FFF500';
        default:
            return '#B0B0B0';
    }
}

const ContainerStyle = {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
    padding: 10,
    paddingLeft: 5,
    margin: 0
};

const StandingsRow: React.StatelessComponent<Props> = (props) => {
    const TeamStyle = {
        backgroundColor: getBackgroundColor(props.team),
        paddingRight: 10,
        marginRight: 10,
    };

    return(
        <View style={ContainerStyle} key={uuid()}>
            <View style={{ minWidth: 35, minHeight: 35 }}>
                <Text style={{
                    padding: 5,
                    color: 'black',
                    fontSize: 18,
                    fontFamily: "TitilliumWeb-Bold",
                    textAlign: 'center',
                }}>
                    {props.position}
                </Text>
            </View>
            <View style={TeamStyle} />
            <View style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                width: '80%',
            }}>
                <Text style={{
                    fontFamily: "TitilliumWeb-Bold",
                    marginLeft: 10,
                }}>
                    { props.name }
                </Text>
                <Text style={{
                    textAlign: 'center',
                    fontFamily: 'TitilliumWeb-Bold'
                }}>
                    { props.points }
                </Text>
            </View>
        </View>
    );
};

export default StandingsRow;
