import * as React from 'react';
import Trophy from "../../svg/Trophy/Trophy";
import Flag from "../../svg/Flag/Flag";
import Home from "../../svg/Home/Home";
import Team from "../../svg/Team/Team";

interface Props {
    name: string;
    color: string;
    width: number;
    height: number;
}

const Sprite: React.StatelessComponent<Props> = (props) => {
    switch(props.name) {
        case 'trophy':
            return <Trophy fill={props.color} height={props.height} width={props.width} />;
        case 'flag':
            return <Flag fill={props.color} height={props.height} width={props.width} />;
        case 'home':
            return <Home fill={props.color} height={props.height} width={props.width} />;
        case 'team':
            return <Team fill={props.color} height={props.height} width={props.width} />;
        default:
            return null;
    }
};

export default Sprite;
