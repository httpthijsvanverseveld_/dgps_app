import * as React from 'react';
import { View, Text } from 'react-native';
import Sprite from "../Sprite";

interface Props {
    name: string;
    badgeCount: number;
    focused: string;
    color: string;
}

class NavigationBadge extends React.Component<Props> {
    render() {
        const { name, badgeCount, color } = this.props;
        return (
            <View style={{ width: 24, height: 24, margin: 5 }}>
                <Sprite name={name} color={color} height={24} width={24} />
                { badgeCount > 0 && (
                    <View style={{
                        position: 'absolute',
                        right: -6,
                        top: -3,
                        backgroundColor: 'red',
                        borderRadius: 6,
                        width: 12,
                        height: 12,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold' }}>{badgeCount}</Text>
                    </View>
                )}
            </View>
        );
    }
}

export default NavigationBadge;
