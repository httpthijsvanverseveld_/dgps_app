import * as React from 'react';
import { View } from 'react-native';
import { H1 } from 'native-base';
import {Colors} from "../../utils/colors";

const ContainerStyle = {
    backgroundColor: Colors.red,
    paddingTop: 20,
    paddingBottom: 15,
    paddingLeft: 35,
    paddingRight: 35,
};

const TitleStyle = {
    color: 'white',
    fontFamily: 'TitilliumWeb-SemiBold'
};

interface Props {
    title: string;
}

const Title: React.StatelessComponent<Props> = (props) => (
    <View style={ContainerStyle}>
        <H1 style={TitleStyle}>{props.title}</H1>
    </View>
);

export default Title;
