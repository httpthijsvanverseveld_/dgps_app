import * as React from "react";
import {Button, Text} from "react-native";
import {Colors} from "../utils/colors";
import { Container } from 'native-base';
import Title from "../components/Title/Title";

const RacesStyle = {
    backgroundColor: Colors.dark,
    color: 'white',
};

class Races extends React.Component {
    render() {
        return (
            <Container style={RacesStyle}>
                <Title title="Races"/>
                <Text>Races Screen</Text>
                <Button title="Go to Standings" onPress={() => this.props.navigation.navigate('Standings')}/>
            </Container>
        );
    }
}

export default Races;
