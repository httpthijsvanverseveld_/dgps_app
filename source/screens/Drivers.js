import * as React from "react";
import { Container, Tabs, Tab } from 'native-base';
import StandingsList from "../components/StandingsList/StandingsList";
import {Colors} from "../utils/colors";
import Title from "../components/Title/Title";

const TabStyles = { backgroundColor: Colors.red };
const ActiveTabStyles = { backgroundColor: Colors.red };
const TextStyles = { fontFamily: "TitilliumWeb-Light", color: 'white' };
const ActiveTextStyles = { fontFamily: "TitilliumWeb-Bold", color: 'white' };

class Drivers extends React.Component {
    render() {
        return (
            <Container style={{ backgroundColor: Colors.dark }}>
                <Title title="Drivers"/>
                <Tabs>
                    <Tab heading="GP1" tabStyle={TabStyles} activeTabStyle={ActiveTabStyles} textStyle={TextStyles} activeTextStyle={ActiveTextStyles}>
                        <StandingsList type="drivers" league="gp1"/>
                    </Tab>
                    <Tab heading="GP2" tabStyle={TabStyles} activeTabStyle={ActiveTabStyles} textStyle={TextStyles} activeTextStyle={ActiveTextStyles}>
                        <StandingsList type="drivers" league="gp2"/>
                    </Tab>
                    <Tab heading="GP3" tabStyle={TabStyles} activeTabStyle={ActiveTabStyles} textStyle={TextStyles} activeTextStyle={ActiveTextStyles}>
                        <StandingsList type="drivers" league="gp3"/>
                    </Tab>
                    <Tab heading="F1" tabStyle={TabStyles} activeTabStyle={ActiveTabStyles} textStyle={TextStyles} activeTextStyle={ActiveTextStyles}>
                        <StandingsList type="drivers" league="f1"/>
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}

export default Drivers;
