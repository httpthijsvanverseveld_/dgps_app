import * as React from "react";
import {View, StatusBar} from "react-native";
import Title from "../components/Title/Title";
import {Colors} from "../utils/colors";
import {Container} from "native-base";

const ContainerStyle = {
    backgroundColor: Colors.dark,
    height: '100%',
    width: '100%'
};

class Home extends React.Component {
    render() {
        return (
            <Container style={ContainerStyle}>
                <StatusBar backgroundColor={Colors.red} />
                <Title title="Home"/>
            </Container>
        );
    }
}

export default Home;
