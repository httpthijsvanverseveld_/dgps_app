import React from 'react';
import Svg, { Path } from 'react-native-svg';

const Position = props => (
    <Svg width={45} height={35} {...props}>
        <Path
            data-name="Path 3"
            d="M0 35L16.494 0H45L28.5 35z"
            fill="currentColor"
        />
    </Svg>
);

export default Position;
